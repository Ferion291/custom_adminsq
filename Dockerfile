FROM python:3.8

WORKDIR /cms

COPY requirements.txt ./
RUN apt-get update && apt-get install -y \
gettext 
RUN pip install --no-cache-dir -r requirements.txt

COPY static ./static
COPY . .

CMD [ "gunicorn", "--bind", "0.0.0.0", "-p", "8000", "solidcms__proj.wsgi"  ]
