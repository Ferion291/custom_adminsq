from django.db import models
from django.core.validators import FileExtensionValidator

# Create your models here.
class Social(models.Model):
    key = models.CharField(max_length=100,unique=True)
    value = models.CharField(max_length=100, null=True,blank=True)
    icon = models.FileField(upload_to = 'social_icons',null =True,blank=True , validators=[FileExtensionValidator(['svg' , 'jpg','png' , 'jpeg'])])
    enable_for_site = models.BooleanField(default=True)


    class Meta:
        verbose_name = "Social Network"

    def __str__(self):
        return self.key


class StaticWord(models.Model):
    key = models.CharField(max_length=100,unique=True)
    value = models.CharField(max_length=100, null=True,blank=True)
    enable_for_site = models.BooleanField(default=True)


    class Meta:
        verbose_name = "Static Word"

    def __str__(self):
        return self.key