from django.contrib import admin
from custom_admin_app.models import Admin_Contact, Hero

from root_setting.models import Social, StaticWord

# Register your models here.
admin.site.register(Social)
admin.site.register(StaticWord)
admin.site.register(Admin_Contact) 
admin.site.register(Hero)