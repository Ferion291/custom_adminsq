from modeltranslation.translator import translator, TranslationOptions
from root_setting.models import StaticWord


class StaticWordTranslationOptions(TranslationOptions):
    fields = ('value',)

translator.register(StaticWord, StaticWordTranslationOptions)