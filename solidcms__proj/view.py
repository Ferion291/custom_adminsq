from django.views.generic import View
from django.shortcuts import render

from django.utils import translation
from django.shortcuts import redirect
from django.conf import settings
from navbar.models import LinkNavbar
from django.shortcuts import get_object_or_404, redirect, render
from root_setting.models import Social, StaticWord
from static_page_app.models import Contact_us


class BaseView(View):
    data = dict()
    template = None
    def get(self, request):
        return render(request, self.template, self.data)
    

    def static_translates(self):
        data_for_footer = StaticWord.objects.values_list()
        for data in data_for_footer:
            self.data[str(data[1])] = data[2]
        return self.data
    
    def navbar(self):
        navbar_elements = LinkNavbar.objects.all().filter(enable_for_site=True).order_by('ranking')
        nav_list = []



        nav_data = {}
        nav_list = []
        for navbar in navbar_elements:
            nav_list = [navbar.title, navbar.url ]
            nav_data[navbar] = nav_list
        self.data['navigations'] = nav_data
        return self.data

    def footer_socials(self):
        socials = Social.objects.filter(enable_for_site=True)
        social_data = {}
        social_list = []
        for social in socials:
            social_list = [social.key, social.value , social.icon]
            social_data[social.key] = social_list
        self.data['socials'] = social_data
        return self.data


    def footer_contacts(self):
        contact_us_data_footer = get_object_or_404(Contact_us)
        self.data['phone1'] = contact_us_data_footer.phone1
        self.data['address'] = contact_us_data_footer.address
        return self.data