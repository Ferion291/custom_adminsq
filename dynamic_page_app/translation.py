from .models import ClientInfo
from modeltranslation.translator import translator, TranslationOptions
from dynamic_page_app.models import *


class ClientInfoTranslationOptions(TranslationOptions):
    fields = ('title',)

translator.register(ClientInfo, ClientInfoTranslationOptions)


class CategoryItemTranslationOptions(TranslationOptions):
    fields = ('title',)

translator.register(CategoryItem, CategoryItemTranslationOptions)


class ProductItemTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(ProductItem, ProductItemTranslationOptions)

class FeatureTranslationOptions(TranslationOptions):
    fields = ('title', )

translator.register(Feature, FeatureTranslationOptions)
