from django.contrib import admin

from dynamic_page_app.models import *

# Register your models here.
admin.site.register(ClientInfo)


class CategoryItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'order')
admin.site.register(CategoryItem, CategoryItemAdmin)




class ProductItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'category')
admin.site.register(ProductItem, ProductItemAdmin)


admin.site.register(Feature)


class ProductPhotosAdmin(admin.ModelAdmin):
    list_display = ('product', 'image')
admin.site.register(ProductPhotos, ProductPhotosAdmin)

