# Generated by Django 3.2 on 2022-08-21 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_page_app', '0014_auto_20220821_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productitem',
            name='future',
            field=models.ManyToManyField(blank=True, default='NULL', to='dynamic_page_app.Future'),
        ),
    ]
