from django.apps import AppConfig


class DynamicPageAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dynamic_page_app'
