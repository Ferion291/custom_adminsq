upload.onchange = (evt) => {
  const [file] = upload.files;
  if (file) {
    uploadedImg.src = URL.createObjectURL(file);
  }
};

uploadedFavicon.onchange = (evt) => {
  const [file] = uploadedFavicon.files;
  if (file) {
    uploadedIcon.src = URL.createObjectURL(file);
  }
};
