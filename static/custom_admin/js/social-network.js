facebook.onchange = evt => {
    const [file] = facebook.files
    if (file) {
    facebookImage.src = URL.createObjectURL(file)
    }
}

instagram.onchange = evt => {
    const [file] = instagram.files
    if (file) {
    instagramImage.src = URL.createObjectURL(file)
    }
}

linkedin.onchange = evt => {
    const [file] = linkedin.files
    if (file) {
    linkedinImage.src = URL.createObjectURL(file)
    }
}

twitter.onchange = evt => {
    const [file] = twitter.files
    if (file) {
    twitterImage.src = URL.createObjectURL(file)
    }
}