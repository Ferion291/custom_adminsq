from modeltranslation.translator import translator, TranslationOptions
from static_page_app.models import About_us, Contact_us, Request_Form

class AboutusTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )

translator.register(About_us, AboutusTranslationOptions)



class ContactusTranslationOptions(TranslationOptions):
    fields = ('location_title', 'page_title', 'address', 'call_us_title', 'phone_title', 'fax_title', 'connect_online_title', 'email_title',  )

translator.register(Contact_us, ContactusTranslationOptions)


class RequestFormTranslationOptions(TranslationOptions):
    fields = ('fullname_placeholder', 'phone_placeholder', 'email_placeholder', 'message_placeholder', 'button_title', )

translator.register(Request_Form, RequestFormTranslationOptions)
