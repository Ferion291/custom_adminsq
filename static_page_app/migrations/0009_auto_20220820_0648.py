# Generated by Django 3.2 on 2022-08-20 02:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('static_page_app', '0008_auto_20220820_0615'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contact_us',
            old_name='other',
            new_name='connect_online_title',
        ),
        migrations.AddField(
            model_name='contact_us',
            name='email',
            field=models.CharField(blank=True, default='', max_length=100, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='contact_us',
            name='email_title',
            field=models.CharField(blank=True, default='', max_length=100, null=True, unique=True),
        ),
    ]
