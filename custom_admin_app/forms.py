from django.forms import ModelForm
from root_setting.models import StaticWord

class AddWordForm(ModelForm):
    class Meta:
        model=StaticWord
        fields='__all__'