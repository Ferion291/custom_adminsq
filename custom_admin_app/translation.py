from modeltranslation.translator import translator, TranslationOptions
from custom_admin_app.models import Admin_Contact , Hero


class Admin_ContactTranslationOptions(TranslationOptions):
    fields = ('value','page_title',)

translator.register(Admin_Contact,Admin_ContactTranslationOptions)




