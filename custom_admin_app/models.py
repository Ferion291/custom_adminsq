from cgitb import enable
from django.db import models
from django.core.validators import FileExtensionValidator

# Create your models here.
class Social(models.Model):
    key = models.CharField(max_length=100,unique=True)
    value = models.CharField(max_length=100, null=True,blank=True)
    icon = models.FileField(upload_to = 'social_icons',null =True,blank=True , validators=[FileExtensionValidator(['svg' , 'jpg','png' , 'jpeg'])])
    enable_for_site = models.BooleanField(default=True)


    class Meta:
        verbose_name = "Social Network"

    def __str__(self):
        return self.key


class StaticWord(models.Model):
    key = models.CharField(max_length=100,unique=True)
    value = models.CharField(max_length=100, null=True,blank=True)
    enable_for_site = models.BooleanField(default=True)


    class Meta:
        verbose_name = "Static Word"

    def __str__(self):
        return self.key

class Admin_Contact(models.Model):
    value = models.CharField(max_length=100, null=True,blank=True)
    key = models.CharField(max_length=100,unique=True)
    page_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    location_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    address = models.CharField(max_length=100,unique=True, default='', blank=True)
    call_us_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    fax_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone1 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    connect_online_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    email_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    mail1 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)

    class Meta:
        verbose_name = "Admin Contact Page"

    def __str__(self):
        return "Edit Admin-Contact Page"

class Hero(models.Model):
    value = models.CharField(max_length=100, null=True,blank=True)
    key = models.CharField(max_length=100,unique=True)
    line_1 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    line_2 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    line_3 = models.CharField(max_length=100,unique=True, default='', blank=True)
    line_4 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)

    class Meta:
        verbose_name = "Admin Hero Page"

    def __str__(self):
        return "Edit Admin-Hero Page"
    
class Admin_Request_Form(models.Model):
    name_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    surname_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone_placeholder = models.CharField(max_length=100,unique=True, default='', blank=True)
    email_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    message_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    button_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)

    class Meta:
        verbose_name = "Request Page"

    def __str__(self):
        return "Request Page"

