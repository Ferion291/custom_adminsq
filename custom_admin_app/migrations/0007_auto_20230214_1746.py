# Generated by Django 3.2 on 2023-02-14 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_admin_app', '0006_auto_20230214_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='admin_contact',
            name='value_az',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='admin_contact',
            name='value_en',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='admin_contact',
            name='value_ru',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
