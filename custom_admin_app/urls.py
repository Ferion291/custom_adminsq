"""solidcms__proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import url
from custom_admin_app import views
from custom_admin_app.views import *
from dynamic_page_app.views import *

from static_page_app.views import *
from django.utils.translation import gettext_lazy as _

urlpatterns = i18n_patterns(

    
    path('admin/', admin.site.urls), 
    path('custom_admin/static_words/', StaticWordsView.as_view(), name="StaticWordsView"),
    path('custom_admin/static_words/add_word/', AddWordView.as_view(), name="AddWordView"),
    path('custom_admin/static_words/create_word', CreateWordView.as_view(), name="CreateWordView"),
    path('custom_admin/static_words/edit_static_word/<int:pk>/', EditWordView.as_view(), name="EditWordView"),
    path('custom_admin/static_words/delete_word/<int:pk>/', AdminDeleteWordView.as_view(), name="AdminDeleteWordView"),
    path('custom_admin/contact/', AdminContactView.as_view(), name="AdminContactView"),
    path('custom_admin/contact/add/', AdminAddContactView.as_view(), name="AdminAddContactView"),
    path('custom_admin/contact/edit/<int:pk>/', AdminEditContactView.as_view(), name="AdminEditContactView"),
    path('custom_admin/hero/', AdminHeroView.as_view(), name="AdminHeroView"),
    path('custom_admin/steps/', AdminStepsView.as_view(), name="AdminStepsView"),
    path('custom_admin/services/', AdminServicesView.as_view(), name="AdminServicesView"),
    path('custom_admin/portfolio/', AdminPortfolioView.as_view(), name="AdminPortfolioView"),
    path('custom_admin/social/', AdminSocialView.as_view(), name="AdminSocialView"),
    path('custom_admin/social/add', AdminSocialAddView.as_view(), name="AdminSocialAddView"),
    path('custom_admin/social/edit/<int:pk>/', AdminSocialEditView.as_view(), name="AdminSocialEditView"),
    path('custom_admin/all_requests/', AdminAllRequestsView.as_view(), name="AdminAllRequestsView"),
    path('custom_admin/', CustomAdminHomeView.as_view(), name="CustomAdminHomeView"),
    path('i18n/', include('django.conf.urls.i18n')),


  prefix_default_language=True
) + static(prefix=settings.MEDIA_URL , document_root=settings.MEDIA_ROOT) 
   

#path('i18n/', include('django.conf.urls.i18n')),