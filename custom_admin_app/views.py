from django.shortcuts import get_object_or_404, redirect, render 
from django.urls import reverse, reverse_lazy
from solidcms__proj.view import BaseView
from django.views.generic import FormView
from django.views.generic.edit import DeleteView
from custom_admin_app.models import *
from root_setting.models import *
from .forms import AddWordForm


# Create your views here.
class CustomAdminHomeView(BaseView):
    data = dict()
    template = "home.html"

    def get(self, request):
        self.data = dict()

        #Page specific data
        self.data['page_title'] = "Custom Admin Page"
        self.data['page_content'] = "custom_admin_home.content"
        self.data['page_meta_title'] = "Some text"
        return render(request, self.template, self.data)

class StaticWordsView(BaseView):
    data = dict()
    template = "static-words/static_words.html"

    def get(self, request):
        self.data = dict()
        self.data['admin_static_words']=StaticWord.objects.all().filter(enable_for_site=False)
        static_words=StaticWord.objects.all().filter(enable_for_site=True)

        self.data["static_words"]=static_words

        return render(request, self.template, self.data)

class EditWordView(BaseView):
    data=dict()
    template="static-words/edit-word.html"

    def get(self, request, pk):
        self.data=dict()
        #change thistion
        admin_static_words=StaticWord.objects.all().filter(enable_for_site=False)
        for admin_static_w in admin_static_words:
            self.data[str(admin_static_w.key)] = admin_static_w.value

        self.data['static_word'] = get_object_or_404(StaticWord, pk=pk)
        return render(request, self.template, self.data)
    
    def post(self, request, pk):
        self.data=dict()
        #change thistion
        admin_static_words=StaticWord.objects.all().filter(enable_for_site=False)
        for admin_static_w in admin_static_words:
            self.data[str(admin_static_w.key)] = admin_static_w.value



        #form variables here 
        StaticWord.objects.filter(pk=pk).update(value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])

        self.data['static_word'] = get_object_or_404(StaticWord, pk=pk)

        return render(request, self.template, self.data)

class StaticWordCancelView(BaseView):
    template_name = 'static_words.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request, *args, **kwargs):
        # Add your cancel logic here
        return redirect(reverse_lazy('StaticWordsView'))


class AddWordView(BaseView):
    data = dict()
    template = "static-words/add-word.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
    def post(self, request):
        self.data=dict()
        print(request.POST)
        new_word = StaticWord(key =  request.POST['key'], value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])
        new_word.save()

        return render(request, self.template, self.data)
    


class AdminDeleteWordView(DeleteView):

    model=StaticWord

    success_url= '/custom_admin/static_words/'
    template_name="static-words/delete-word.html"



class CreateWordView(FormView):
    data = dict()
    template= "static-words/add-word.html"

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
      
class AdminContactView(BaseView):
    data=dict()
    template="contact/contact.html"

    def get(self, request):
        self.data=dict()
        admin_contacts = Admin_Contact.objects.all()

        self.data['admin_contacts'] = admin_contacts

        return render(request, self.template, self.data)

class AdminAddContactView(BaseView):
    data = dict()
    template = "contact/contact_add.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
    def post(self, request):
        self.data=dict()
        print(request.POST)
        new_contact = Admin_Contact(key =  request.POST['key'], value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])
        new_contact.save()

        return render(request, self.template, self.data)

class AdminEditContactView(BaseView):
    data=dict()
    template="contact/contact_edit.html"

    def get(self, request, pk):
        self.data=dict()
        #change thistion
      

        self.data['edit_contact'] = get_object_or_404(Admin_Contact, pk=pk)
        return render(request, self.template, self.data)
    
    def post(self, request, pk):
        self.data=dict()
        #change thistion

        #form variables here 
        Admin_Contact.objects.filter(pk=pk).update(value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])

        self.data['edit_contact'] = get_object_or_404(Admin_Contact, pk=pk)

        return render(request, self.template, self.data)
    

    
class AdminHeroView(BaseView):
    data=dict()
    template="hero/hero.html"

    def get(self, request):
        self.data=dict()
        heros = Hero.objects.all()
        print(heros)
        self.data['heros'] = heros
        return render(request, self.template, self.data)

class AdminHeroAddView(BaseView):
    data = dict()
    template = "hero/hero_add.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
    def post(self, request):
        self.data=dict()
        print(request.POST)
        add_line = Hero(key =  request.POST['key'], value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])
        add_line.save()

        return render(request, self.template, self.data)

class AdminHeroEditView(BaseView):
    data=dict()
    template="hero/hero_edit.html"

    def get(self, request, pk):
        self.data=dict()

        self.static_translates()
        #change thistion
        hero=Hero.objects.all()


        self.data['hero'] = get_object_or_404(Hero, pk=pk)
        return render(request, self.template, self.data)
    
    def post(self, request, pk):
        self.data=dict()
        #change thistion
        hero=Hero.objects.all()
        for hero in hero:
            self.data[str(hero.key)] = hero.value



        #form variables here 
        Hero.objects.filter(pk=pk).update(value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])

        self.data['hero'] = get_object_or_404(Hero, pk=pk)

        return render(request, self.template, self.data)

class AdminStepsView(BaseView):
    data=dict()
    template="steps/steps.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
class AdminServicesView(BaseView):
    data=dict()
    template="services/services.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
class AdminPortfolioView(BaseView):
    data=dict()
    template="portfolio/portfolio.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)

class AdminSocialView(BaseView):
    data=dict()
    template="social-network/social.html"

    def get(self, request):
        self.data=dict()
        self.data['admin_social_networks']=Social.objects.all().filter(enable_for_site=False)
        social_network=Social.objects.all().filter(enable_for_site=True)
        self.data["social_network"]=social_network

        return render(request, self.template, self.data)

class AdminSocialAddView(BaseView):
    data = dict()
    template = "social-network/social_add.html"

    def get(self, request):
        self.data=dict()

        return render(request, self.template, self.data)
    
    def post(self, request):
        self.data=dict()
        new_network = Social(key =  request.POST['key'], value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'], icon=request.Post['icon'])
        new_network.save()

        return render(request, self.template, self.data)
    
class AdminSocialEditView(BaseView):
    data=dict()
    template="social-network/social_edit.html"

    def get(self, request, pk):
        self.data=dict()
        #change thistion
        #self.data['admin_social_network']=Social.objects.all().filter(enable_for_site=False)
        #for admin_social_n in admin_social_network:
        #    self.data[str(admin_social_n.key)] = admin_social_n.value

        self.data['social_network'] = get_object_or_404(Social, pk=pk)
        return render(request, self.template, self.data)
    
    def post(self, request, pk):
        self.data=dict()
        #change thistion
        admin_social_network=Social.objects.all().filter(enable_for_site=False)
        for admin_social_n in admin_social_network:
            self.data[str(admin_social_n.key)] = admin_social_n.value

        Social.objects.filter(pk=pk).update(value_en =  request.POST['title_en'], value_az = request.POST['title_az'],
                                            value_ru = request.POST['title_ru'])
        
        return render(request, self.template, self.data)

class AdminSocialDeleteView(DeleteView):

    model=Social
    template_name="social-network/social_delete.html"
    success_url = reverse_lazy('social-network/social.html')
    

    
class AdminAllRequestsView(BaseView):
    data=dict()
    template="all_requests/all_requests.html"

    def get(self, request):
        self.data=dict()
        all_requests=Admin_Request_Form.objects.all()
        self.data['all_requests'] = all_requests

        return render(request, self.template, self.data)

        #Page Specific Data



